from bold_model_classification import NeuralNetwork, bold_classification
from training_bold import Training_classification
# from test_bold import MODEL_CLASSIFICATION

training_data = "data/train2/"
valid_data = "data/valid/"

model = Training_classification()
model.training_bold_classification(training_data, valid_data)

# model = MODEL_CLASSIFICATION()
# model.get_output_model("data/data_test", "data/result")