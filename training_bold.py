import os
import torch
from torch.utils.data import DataLoader
import torchvision.transforms as T
from bold_model_classification import NeuralNetwork, bold_classification

class Training_classification(object):

    def __init__(self):
        super().__init__()
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.model = NeuralNetwork().to(self.device)
        self.learning_rate = 0.001
        self.loss_fn = torch.nn.CrossEntropyLoss()  # Softmax is internally computed.
        self.optimizer = torch.optim.Adam(params=self.model.parameters(), lr=self.learning_rate)
        self.epochs = 30

    def get_training_data(self, data_folder, vali_folder):

        DIR_TRAIN = data_folder
        DIR_VALID = vali_folder
        classes = os.listdir(DIR_TRAIN)

        train_count = 0
        valid_count = 0
        for _class in classes:
            train_count += len(os.listdir(DIR_TRAIN + _class))
            valid_count += len(os.listdir(DIR_VALID + _class))

        print("Total train images: ", train_count)
        print("Total valid images: ", valid_count)

        train_imgs = []
        valid_imgs = []

        for _class in classes:

            for img in os.listdir(DIR_TRAIN + _class):
                train_imgs.append(DIR_TRAIN + _class + "/" + img)

            for img in os.listdir(DIR_VALID + _class):
                valid_imgs.append(DIR_VALID + _class + "/" + img)

        class_to_int = {classes[i]: i for i in range(len(classes))}
        return train_imgs, valid_imgs, class_to_int


    def get_transform(self):
        return T.Compose([T.ToTensor()])

    def get_data_loader(self, train_imgs, valid_imgs, class_to_int, tranform):
        train_dataset = bold_classification(train_imgs, class_to_int, tranform)
        valid_dataset = bold_classification(valid_imgs, class_to_int, tranform)
        train_dataloader = DataLoader(train_dataset, batch_size=64, shuffle=True)
        valid_dataloader = DataLoader(valid_dataset, batch_size=64, shuffle=True)
        return train_dataloader, valid_dataloader



    def train(self, dataloader, model, loss_fn, optimizer):
        size = len(dataloader.dataset)
        model.train()
        for batch, (X, y) in enumerate(dataloader):
            X, y = X.to(self.device), y.to(self.device)
            pred = model(X)
            loss = loss_fn(pred, y)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch % 50 == 0:
                loss, current = loss.item(), batch * len(X)
                print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

    def test(self, dataloader, model, loss_fn):
        size = len(dataloader.dataset)
        num_batches = len(dataloader)
        model.eval()
        test_loss, correct = 0, 0
        with torch.no_grad():
            for X, y in dataloader:
                X, y = X.to(self.device), y.to(self.device)
                pred = model(X)
                test_loss += loss_fn(pred, y).item()
                correct += (pred.argmax(1) == y).type(torch.float).sum().item()
        test_loss /= num_batches
        correct /= size
        print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")


    def training_bold_classification(self, training_dir, valid_dir):
        train_imgs, valid_imgs, class_to_int = self.get_training_data(training_dir, valid_dir)
        train_dataloader, valid_dataloader = self.get_data_loader(train_imgs, valid_imgs, class_to_int, self.get_transform())

        for t in range(self.epochs):
            print(f"Epoch {t+1}\n-------------------------------")
            # train_dataloader, valid_dataloader = self.get_data_loader()
            self.train(train_dataloader, self.model, self.loss_fn, self.optimizer)
            self.test(valid_dataloader, self.model, self.loss_fn)
        print("Done!")
        print("Model's state_dict:")

        torch.save(self.model.state_dict(), 'model/model_weights_11_1.pth')
